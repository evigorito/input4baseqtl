
<!-- README.md is generated from README.Rmd. Please edit that file -->

# input4baseqtl

This package provides a set of useful functions to prepare inputs for
[baseqtl](https://gitlab.com/evigorito/baseqtl)

## System requirements

    R versions >= 3.4.0.

## Install baseqtl from [GitLab](https://gitlab.com):

``` r
install.packages("devtools") # if you don't already have the package
library(devtools)
devtools::install_git(url = "https://gitlab.com/evigorito/input4baseqtl.git"
```
